import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { OrderListComponent } from './views/order/order-list/order-list.component';
import { RequestOrderComponent } from './views/order/request-order/request-order.component';


const routes: Routes = [
  {
    path:'',
    redirectTo: 'order/list', 
    pathMatch: 'full' 
  },
  {
    path:'order/list',
    component: OrderListComponent,

  },
  {
    path:'order/request',
    component: RequestOrderComponent,
    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
