import { Component, OnInit } from '@angular/core';
import { ApiOrderService } from 'src/app/shared/services/api/api-order.service';
import { SubscriptionService } from 'src/app/shared/services/subcription.service';
import { MatTableDataSource } from '@angular/material';
import { AppLoaderService } from 'src/app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  orderList: any ;
  orderColumns: string[] = [
    'destination-user',
    'destination-phone',
    'destination-district',
    'origin-user',
    'origin-phone',
    'origin-district',
    'transporter-type',
    'transporter-capacity',
    'transporter-commodity',
    'transporter-qty',
    'transporter-ref', 
    // 'transporter-unit',
    'transporter-delivery', 
    'transporter-note',

  ];
  constructor(
    private apiOrderService : ApiOrderService,
    private subscriptionService : SubscriptionService,
    private appLoaderService : AppLoaderService
  ) { 
    this.orderList = new MatTableDataSource();
  }

  ngOnInit() {
   this.appLoaderService.open();
    this.getList();
  }

  private getList(){
    this.subscriptionService.addSubscription(this.apiOrderService.getAll().subscribe(res =>{
      if(res){
        let order = JSON.parse(localStorage.getItem("order"));
        let temp = [];
        order ? order.forEach(e => {
          res.forEach(f =>{
            if(e != f){
              console.log(true)
              temp.push(e);
            }
          } )
        }) : localStorage.setItem("order" , JSON.stringify(this.orderList.data));
        this.orderList.data = temp;
        this.appLoaderService.close();

      }
    }))
  }
} 
