import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { SubscriptionService } from 'src/app/shared/services/subcription.service';
import { ApiDistrictService } from 'src/app/shared/services/api/api-district.service';
import { Subscription, Observable, of } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ApiTruckTypeService } from 'src/app/shared/services/api/api-truck-type.service';
import { ApiCommodityService } from 'src/app/shared/services/api/api-commodity.service';
import { ApiCapacityService } from 'src/app/shared/services/api/api-capacity.service';
import { ComponentType } from '@angular/cdk/portal';
import { MatDialog } from '@angular/material';
import { ReferensiDialogComponent } from './referensi-dialog/referensi-dialog.component';
import { DatePipe } from '@angular/common';
import { AppLoaderService } from 'src/app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-request-order',
  templateUrl: './request-order.component.html',
  styleUrls: ['./request-order.component.scss']
})
export class RequestOrderComponent implements OnInit {
  formGroupDestination: FormGroup;
  formGroupOrigin: FormGroup;
  formGroupTransporter: FormGroup;
  destinationDistrictList = [];
  originDistrictList = [];
  tructTypeList = [];
  unitList = [];
  capacityList = [];
  commodityList = [];
  noRefList = [];
  minDate = new Date();
  constructor(
    private formBuilder: FormBuilder,
    private subscriptionService: SubscriptionService,
    private apiDistrictService: ApiDistrictService,
    private apiTypeTruckService: ApiTruckTypeService,
    private apiCommodityService: ApiCommodityService,
    private apiCapacityService: ApiCapacityService,
    private appLoaderService : AppLoaderService,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.formGroupDestination = this.formBuilder.group(this.getFormGroupDestination());
    this.formGroupOrigin = this.formBuilder.group(this.getFormGroupOrigin());
    this.formGroupTransporter = this.formBuilder.group(this.getFormGroupTransporter());
    this.getTruckType();
    this.formGroupDestination.get('district').valueChanges.pipe(debounceTime(2000)).subscribe(res => {
      if (res) {
        this.subscriptionService.addSubscription(this.apiDistrictService.getOnceWithParam({ keyword: res }).subscribe(res => {
          if (res) {
            this.destinationDistrictList = res;
            this.cdr.detectChanges();
          } else {
            this.destinationDistrictList = [];
          }
        }))
      }
    });
    this.formGroupOrigin.get('district').valueChanges.pipe(debounceTime(2000)).subscribe(res => {
      if (res) {
        this.subscriptionService.addSubscription(this.apiDistrictService.getOnceWithParam({ keyword: res }).subscribe(res => {
          if (res) {
            this.originDistrictList = res;
            this.cdr.detectChanges();
          } else {
            this.originDistrictList = [];
          }
        }))
      }
    })
  }

  //get dialog width 
  getDialogWidth() {
    return '60%';
  }
  
  //modal call compoonent
  openSiteDialog<T>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, data?): Observable<any> {
    const dialogRef = this.dialog.open(componentOrTemplateRef, { width: this.getDialogWidth(), disableClose: true, data: data });
    return dialogRef.beforeClose();
  }

  //get truck type
  getTruckType() {
    this.subscriptionService.addSubscription(this.apiTypeTruckService.getAll().subscribe(res => {
      if (res) {
        this.tructTypeList = res;
      }
    }))
  }

  //change select truct type 
  ChangeTruckTypeEvent(val: any) {
    if (val) {
      this.unitList = val.unit;
      this.getCapacity(val.truck_type_id);
      this.getCommodity(val.truck_type_id);
      this.cdr.detectChanges();  
    }
  }

  //get capacity
  getCapacity(id){
    this.subscriptionService.addSubscription(this.apiCapacityService.getOnceWithParam({ id: id}).subscribe(res => {
      if (res) {
        console.log(res);
        this.capacityList = res;
        this.cdr.detectChanges();
      }
    }));
  }

  //get commodity 
  getCommodity(id){
    this.subscriptionService.addSubscription(this.apiCommodityService.getOnceWithParam({ id: id}).subscribe(res => {
      if (res) {
        console.log(res);
        this.commodityList = res;
        this.cdr.detectChanges();
      }
    }))
  }

  //change unit 
  ChangeUnitEvent(val: any) {
    if (val) {
      this.formGroupTransporter.get("unit_2").setValue(val);
    }
  }

  //get form group destination
  getFormGroupDestination(): any {
    return {
      user: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      phone: [null, [Validators.required, Validators.minLength(9), Validators.maxLength(12), this.prefixValidator]],
      district: [null, [Validators.required]],
      address: [null, [Validators.required]]
    }
  }

  //get form group origin
  getFormGroupOrigin(): any {
    return {
      user: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      phone: [null, [Validators.required, Validators.minLength(9), Validators.maxLength(12), this.prefixValidator]],
      district: [null, [Validators.required]],
      address: [null, [Validators.required]]
    }
  }

  //get form group Transporter
  getFormGroupTransporter(): any {
    return {
      truck_type: [null, [Validators.required]],
      capacity: [null, [Validators.required]],
      unit: [null, [Validators.required]],
      category: [null, [Validators.required]],
      qty_order: [null, [Validators.required , Validators.min(1)]],
      unit_2: [null, [Validators.required]],
      no_ref: [null, [Validators.required]],
      start_delivery_date: [null, [Validators.required]],
      note: [null, [Validators.required]]
    }
  }

  //add no Ref
  addNoRef() {

    this.subscriptionService.addSubscription(this.openSiteDialog(ReferensiDialogComponent, this.noRefList).subscribe(res => {
      console.log(res);
      if (res) {
        this.noRefList = res;
        if(this.noRefList.length > 0 ){
          this.formGroupTransporter.get("no_ref").patchValue(this.noRefList.length + " Nomor Referensi")
        }
      }
    }));
  }

  //save data 
  save(){
    this.appLoaderService.open();
    let destination = this.formGroupDestination.getRawValue();
    let origin      = this.formGroupOrigin.getRawValue();
    let transporter = this.formGroupTransporter.getRawValue();
    let data = {
      destination: {
        user:  destination.user, 
        phone: destination.phone, 
        district: destination.district
      },
      origin: {
        user: origin.user, 
        phone: origin.phone, 
        district: origin.district
      },
      transporter: {
        truck_type: transporter.truck_type['truck_type'], 
        capacity: transporter.capacity, 
        commodity_name: transporter.category['commod_category'],
        note: transporter.note,
        qty_order: transporter.qty_order,
        ref_no: this.noRefList,
        start_delivery_date: new Date (new DatePipe('en_US').transform( transporter.start_delivery_date, 'd MMMM y hh:mm:ss')),
        unit: transporter.unit
      }
    }
    let orderList = [];
    let order = JSON.parse(localStorage.getItem("order"));
    orderList = order ? order : [];
    orderList.push(data);
    localStorage.setItem("order" , JSON.stringify(orderList));
    this.appLoaderService.close();
    window.location.href = window.location.origin + '/order/list';
  }

  //validator input phone on keypress
  numberOnly(event): boolean {
    console.log(this.formGroupDestination.dirty);
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  //validator prefix
  prefixValidator(control: FormControl) {

    if (control.value) {
      let val = control.value.toString();
      let firstChar = val.substring(0, 1);
      console.log(firstChar);
      if (firstChar != "8") {
        return { 'prefix': true };
      }
    }
    return null;
  }


}
