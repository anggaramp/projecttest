import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-referensi-dialog',
  templateUrl: './referensi-dialog.component.html',
  styleUrls: ['./referensi-dialog.component.scss']
})
export class ReferensiDialogComponent implements OnInit {
  noRefList = [];
  formGroup : FormGroup;
  error : boolean = false;
  errorText = "";
  constructor(
    public dialogRef : MatDialogRef<ReferensiDialogComponent>,
    private formBuilder : FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data

  ) { 
    if(data){
      this.noRefList = data;
    }
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group(this.getFormGroup());
    this.formGroup.get('no').valueChanges.pipe(debounceTime(1000)).subscribe(res =>{
        var ind = this.noRefList.map(function(e) { return e;}).indexOf(res);
        if(ind >= 0){
          this.error = true;
          this.errorText = "Nomor Referensi tidak boleh sama"
        }
    })
  }

  getFormGroup():any{
    return{
      no : [null, [Validators.required , Validators.minLength(5), Validators.maxLength(50), ]]
    }
  }

  addRef(){
    let val = this.formGroup.get("no").value;
    if(this.noRefList.length <= 0){
      this.noRefList.push(val);
      this.formGroup.get("no").patchValue(null);

    }
    else if(this.noRefList.length > 0 && this.noRefList.length < 10){
      var ind = this.noRefList.map(function(e) { return e;}).indexOf(val);
        if (ind < 0){
          this.error = false;
          this.noRefList.push(val);
          this.formGroup.get("no").patchValue(null);

        }else{
          this.error = true;
          this.errorText = "Nomor Referensi tidak boleh sama"
        }
    }else{
      this.error = true;
      this.errorText = "Hanya bisa input maksimum 10 data"
    }
    console.log(this.noRefList);
    // }else if(this.noRefList.length >= 10){
    //   
    // }
   
  }
  deleteRef(val){
    let tempExecutor = [];
    var ind = this.noRefList.map(function(e) { return e;}).indexOf(val);
    if(ind >= 0){
      this.noRefList.splice(ind,1);
    }
  
  }
  // validatorNoRef(control: FormControl) {

  //   if (control.value && this.noRefList.length) {
  //     var ind = this.noRefList.map(function(e) { return e;}).indexOf(control.value);
  //     if(ind >= 0){
  //       return { 'noRef': true };
  //     }
  //   }
  //   return null;
  // }

}
