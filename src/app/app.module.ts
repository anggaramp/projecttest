import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderListComponent } from './views/order/order-list/order-list.component';
import { RequestOrderComponent } from './views/order/request-order/request-order.component';
import { ReferensiDialogComponent } from './views/order/request-order/referensi-dialog/referensi-dialog.component';
import { ErrorMessageComponent } from './shared/components/error-message/error-message.component';
import { ApiOrderService } from './shared/services/api/api-order.service';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule, MatPaginator, MatPaginatorModule, MatTabsModule, MatIconModule, MatInputModule, MatSelectModule, MatAutocompleteModule, MatButtonModule, MatDialogModule, MatDatepicker, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppLoaderComponent } from './shared/services/app-loader/app-loader.component';
@NgModule({
  declarations: [
    AppComponent,
    OrderListComponent,
    RequestOrderComponent,
    ReferensiDialogComponent,
    ErrorMessageComponent,
    AppLoaderComponent
  ],
  entryComponents:[
    ReferensiDialogComponent,
    AppLoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
