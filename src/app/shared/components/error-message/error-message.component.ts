import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from '../../services/validation.service';


@Component({
  selector: 'app-error-message',
  template: `<small *ngIf="errorMessage !== null" class="form-error-msg">{{errorMessage}}</small>`,
  styleUrls:['./error-message.component.scss']
  
})
export class ErrorMessageComponent {
  @Input() control : FormControl;
  @Input() fieldName : string;
  constructor() { }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(this.fieldName, propertyName, this.control.errors[propertyName]);
      }
    }

    return null;
  }

}