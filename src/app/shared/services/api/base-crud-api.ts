import { BaseApi } from "./base-api";
import { Observable } from "rxjs";

export abstract class BaseCrudApi extends BaseApi {
    protected abstract getBaseURL(): string;
	

    public getAll(params?: any): Observable<any[]> {
		return this._getList(this.getFullURL(this.getBaseURL()+'/list'), params) ;
	}
	
	public getAllBySearch(params?: string): Observable<any[]>{
		var rand = (new Date()).getTime();
		return this._getList('http://www.mocky.io/v2/5db10e8c2e00003600505171' + '?time=' + rand);
	}
	public getOnce(uid?: any): Observable<any>{
		return this._getOnce(this.getFullURL(this.getBaseURL()));
	}
	public getOnceWithParam(params?: any): Observable<any>{
		return this._getOnce(this.getFullURL(this.getBaseURL()+'/list'), params);
	}
	public create(requestCreate: any): Observable<any> {
		return this._put(this.getFullURL(this.getBaseURL()), requestCreate);
	}
	// public update(requestUpdate:any , uid?:any): Observable<any> {
	// 	return this._patch(this.getFullURL(this.getBaseURL(), uid), requestUpdate);
	// }
	// public updateWithAction(requestUpdate:any , uid?:any , action? :string): Observable<any> {
	// 	return this._patch(this.getFullURL(this.getBaseURL(), uid , action), requestUpdate);
	// }
	// public delete(uid: string , requestDelete:any): Observable<any> {
	// 	return this._delete(this.getFullURL(this.getBaseURL(), uid), requestDelete);
	// }
}