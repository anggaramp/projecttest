import { BaseCrudApi } from './base-crud-api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
	providedIn: 'root'
})
export class ApiTruckTypeService extends BaseCrudApi{
    constructor(protected httpClient: HttpClient){
        super(httpClient)
    }
    public getBaseURL():string{
        return '/truck-type'
    }
}