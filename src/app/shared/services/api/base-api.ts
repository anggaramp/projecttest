import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable} from "rxjs/Rx";
import { map, shareReplay, } from "rxjs/operators";
import { environment } from 'src/environments/environment';

export abstract class BaseApi {
	options:{[k:string]:any} ={};

	// cookieAuth : string;
	// jsonAuth : any ;
    protected constructor(private _httpClient: HttpClient) {
		// this.cookieAuth = Cookie.get('auth')
		// this.jsonAuth = (this.cookieAuth) ? JSON.parse(this.cookieAuth) : null;
    }

    protected getFullURL(url:string){
		var rand = (new Date()).getTime();
		return environment.apiURL + url +'?&times=' + rand;
	}
	
	

    protected _post(url: string, body: any , params?: any): Observable<any> {
		

		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : this.options.params = undefined;

		return this._httpClient.post(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
		}));
	}
	protected _postNoToken(url: string, body: any , params?: any): Observable<any> {

		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : this.options.params = undefined;	
		return this._httpClient.post(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
		}));
	}
	protected _login(url: string, body: any): Observable<any> {
		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);

		return this._httpClient.post(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
		}));
	}

	protected _put(url: string, body: any): Observable<any> {

		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		this.options.params = undefined;
		return this._httpClient.put(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
			}));
	}

	protected _patch(url: string, body: any , params?: any): Observable<any> {
		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : this.options.params = undefined;
		return this._httpClient.patch(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
			}));
	}

	
	protected _patchNoToken(url: string, body: any , params?: any): Observable<any> {

		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : this.options.params = undefined;
		return this._httpClient.patch(url, body , this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
			}));
	}
	protected _delete(url: string , params?:any): Observable<any> {
		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : undefined;
		return this._httpClient.delete(url,this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
			}));
	}

	protected _getList(url: string, params?: any): Observable<any> {
		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		
		(params)? this.options.params = params :undefined;

		return  this._httpClient.get(url, this.options )
			.pipe(map(res => {
				console.log(res);
				return this.parseResponse(res, []);
				// return res;
			}));
	}

	protected _getOne(url: string, params?: any): Observable<any> {
		
		this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);

		(params)? this.options.params = params :undefined;
		
		return this._httpClient.get(url, this.options)
			.pipe(map(res => {
				return this.parseResponse(res, {});
			}));
	}

	protected _getOnce(url: string, params?: any): Observable<any> {
		
		 this.options.headers=new HttpHeaders(
			{
				'Content-Type':  'application/json'
			}
		);
		(params) ? this.options.params = params : this.options.params = undefined;

		return this._httpClient.get(url, this.options)
			.pipe(map(res => {
				return this.parseResponse(res);
			}), shareReplay(1));
	}

	
	private parseResponse(res, defaultValue?) {
		if (res['status'] == 'OK') {

			if (res['data']!= null) {
				return res['data'];
			} else {
				return [];
			}
		}
		return false;
	}
}