import { Injectable } from "@angular/core";
import { Subscription } from "rxjs";


@Injectable({
    providedIn:'root'
})
export class SubscriptionService{
    private subscriptions: Subscription[] = [];

    addSubscription(subscription: Subscription): void{
        this.subscriptions.push(subscription);
    }

    releaseSubscription():void {
        this.subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        })
    }
}