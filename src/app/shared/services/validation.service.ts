import { FormGroup } from "@angular/forms";
import { Injectable } from "@angular/core";
@Injectable({
  providedIn: 'root'
})

export class ValidationService {

    static getValidatorErrorMessage(fieldName:string , validatorName: string, validatorValue?: any) {
        let fieldNameConvert = this.capitalize(fieldName);
        let config = {
          'required':  `${fieldNameConvert} wajib diisi`,
          'email':`${fieldNameConvert} is not valid `,
          'pattern':`${fieldNameConvert} is not valid `,
          'prefix':`${fieldNameConvert} wajib diawali dengan 8`,
          'noRef':`${fieldNameConvert} tidak boleh sama`,
          'invalidCreditCard': 'is invalid credit card number',
          'invalidEmailAddress': 'invalid email address',
          'invalidPassword': 'invalid password. Password must be at least 6 characters long, and contain a number.',
          'minlength': `${fieldNameConvert} Minimum ${validatorValue.requiredLength} karakter`,
          'maxlength': `${fieldNameConvert} Maximum ${validatorValue.requiredLength} karakter`,
          'min':`${fieldNameConvert} Minimum ${validatorValue.min} `,
          'mustMatch': 'invalid your password confirmation'
        };
        return config[validatorName];
      }
    static capitalize(s: string) {
        if (typeof s !== 'string' ) return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
      }
    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
    
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
    
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
      }
}
  
